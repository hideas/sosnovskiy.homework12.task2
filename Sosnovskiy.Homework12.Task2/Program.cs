﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sosnovskiy.Homework12.Task2
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //Set button text from resources
            MessageBoxManager.OK = "Yes";
            MessageBoxManager.Cancel = "No";

            //Register manager
            MessageBoxManager.Register();

            Application.Run(new Puzzle());
        }
    }
}
