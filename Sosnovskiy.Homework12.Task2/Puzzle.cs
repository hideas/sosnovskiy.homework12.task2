﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sosnovskiy.Homework12.Task2
{
    public partial class Puzzle : Form
    {
        // List of checked nodes
        private List<TreeNode> _checkedNodes;

        // Timer variable
        private Timer titleBlinking;

        // Sum variable
        private int _sum;

        public Puzzle()
        {
            InitializeComponent();

            _checkedNodes = new List<TreeNode>();
        }

        private async void Puzzle_Load(object sender, EventArgs e)
        {
            // Hide label text for sum 
            lblSum.Text = "";

            // Setting text blink on form title for 3s
            titleBlinking = new Timer();
            titleBlinking.Interval = 250;
            titleBlinking.Tick += new EventHandler(title_Blink);
            titleBlinking.Start();

            await Task.Delay(3000);

            titleBlinking.Dispose();

            this.Text = "Puzzle";
        }

        private void title_Blink(object sender, EventArgs e)
        {
            this.Text = this.Text.Equals("Select 3 buttons!") ? "Get 50 in total!" : "Select 3 buttons!";
        }

        private void getSum()
        {
            _sum = 0;

            // Getting sum from Listbox items
            foreach (var item in lbSum.Items)
            {
                _sum += int.Parse(item.ToString());
            }
            lblSum.Text = $"Sum: {_sum.ToString()}";
        }

        private void twNumbers_AfterCheck(object sender, TreeViewEventArgs e)
        {
            // If node is checked
            if (e.Node.Checked)
            {
                // Immediately dispose timer
                titleBlinking.Dispose();

                this.Text = "Puzzle";

                // Add nodes to checked list and to Listbox
                _checkedNodes.Add(e.Node);

                lbSum.Items.Add(e.Node.Text);

                // If checked nodes is more then 3
                if (_checkedNodes.Count > 3)
                {
                    // Stop setting check
                    e.Node.Checked = false;
                }
            }
            else
            {
                // Else remove nodes from lists
                _checkedNodes.Remove(e.Node);

                lbSum.Items.Remove(e.Node.Text);
            }

            // Get current sum
            getSum();

            // If sum equals 50 - restart or close
            if (_sum == 50)
            {
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult result = MessageBox.Show("You win! Restart?", $"Sum equals {_sum}!", buttons);

                if (result == DialogResult.Yes)
                {
                    Application.Restart();
                }
                else
                {
                    this.Close();
                }
            }
        }
    }
}
