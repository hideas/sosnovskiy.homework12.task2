﻿namespace Sosnovskiy.Homework12.Task2
{
    partial class Puzzle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("25");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("27");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("3");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("12");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("6");
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("15");
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("9");
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("30");
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("21");
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("19");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Puzzle));
            this.twNumbers = new System.Windows.Forms.TreeView();
            this.lbSum = new System.Windows.Forms.ListBox();
            this.lblSum = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // twNumbers
            // 
            this.twNumbers.CheckBoxes = true;
            this.twNumbers.Cursor = System.Windows.Forms.Cursors.Hand;
            this.twNumbers.Indent = 15;
            this.twNumbers.Location = new System.Drawing.Point(12, 12);
            this.twNumbers.Name = "twNumbers";
            treeNode1.Name = "Node0";
            treeNode1.Text = "25";
            treeNode2.Name = "Node1";
            treeNode2.Text = "27";
            treeNode3.Name = "Node2";
            treeNode3.Text = "3";
            treeNode4.Name = "Node3";
            treeNode4.Text = "12";
            treeNode5.Name = "Node4";
            treeNode5.Text = "6";
            treeNode6.Name = "Node5";
            treeNode6.Text = "15";
            treeNode7.Name = "Node6";
            treeNode7.Text = "9";
            treeNode8.Name = "Node7";
            treeNode8.Text = "30";
            treeNode9.Name = "Node8";
            treeNode9.Text = "21";
            treeNode10.Name = "Node9";
            treeNode10.Text = "19";
            this.twNumbers.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2,
            treeNode3,
            treeNode4,
            treeNode5,
            treeNode6,
            treeNode7,
            treeNode8,
            treeNode9,
            treeNode10});
            this.twNumbers.Scrollable = false;
            this.twNumbers.ShowRootLines = false;
            this.twNumbers.Size = new System.Drawing.Size(87, 167);
            this.twNumbers.TabIndex = 0;
            this.twNumbers.TabStop = false;
            this.twNumbers.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.twNumbers_AfterCheck);
            // 
            // lbSum
            // 
            this.lbSum.FormattingEnabled = true;
            this.lbSum.Location = new System.Drawing.Point(111, 12);
            this.lbSum.Name = "lbSum";
            this.lbSum.Size = new System.Drawing.Size(87, 82);
            this.lbSum.TabIndex = 1;
            // 
            // lblSum
            // 
            this.lblSum.AutoSize = true;
            this.lblSum.Location = new System.Drawing.Point(132, 129);
            this.lblSum.Name = "lblSum";
            this.lblSum.Size = new System.Drawing.Size(31, 13);
            this.lblSum.TabIndex = 2;
            this.lblSum.Text = "Sum:";
            // 
            // Puzzle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(210, 190);
            this.Controls.Add(this.lblSum);
            this.Controls.Add(this.lbSum);
            this.Controls.Add(this.twNumbers);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Puzzle";
            this.Text = "Puzzle";
            this.Load += new System.EventHandler(this.Puzzle_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TreeView twNumbers;
        private System.Windows.Forms.ListBox lbSum;
        private System.Windows.Forms.Label lblSum;
    }
}

